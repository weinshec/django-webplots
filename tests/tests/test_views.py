import webplots
from django.test import TestCase
from django.shortcuts import reverse
from sampleapp.models import RenderMixinModel
from unittest.mock import MagicMock


webplots.fig2base64 = MagicMock()


class TestPreviewMixinInCreateViews(TestCase):

    def test_view_contains_submission_form(self):
        response = self.client.get(reverse("new"))
        self.assertContains(response, "Name")

    def test_view_creates_object_on_submit(self):
        saved_objects = RenderMixinModel.objects.all()

        self.assertEqual(saved_objects.count(), 0)

        self.client.post(reverse("new"), data=dict(name="foobar"))

        self.assertEqual(saved_objects.count(), 1)
        self.assertEqual(saved_objects[0].name, "foobar")

    def test_view_captures_post_request_when_on_preview(self):
        response = self.client.post(
            reverse("new"), data=dict(name="foobar", action="preview"))

        self.assertEqual(response.status_code, 200)

    def test_view_context_holds_preview_when_requested(self):
        response = self.client.post(
            reverse("new"),
            data=dict(name="foobar", action="preview"),
        )
        self.assertIn("rendermixinmodel_preview", response.context)


class TestPreviewMixinInUpdateViews(TestCase):

    def setUp(self):
        self.test_object = RenderMixinModel.objects.create(name="foobar")
        self.test_object.save()

    def test_view_uses_prepopulated_model_form(self):
        response = self.client.get(
            reverse("update", args=[self.test_object.pk]))
        self.assertContains(response, self.test_object.name)

    def test_view_can_update_preview(self):
        response = self.client.post(
            reverse("update", args=[self.test_object.pk]),
            data=dict(name="barfoo", action="preview")
        )
        self.assertIn("rendermixinmodel_preview", response.context)
