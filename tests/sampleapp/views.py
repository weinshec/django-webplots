from django.views.generic import CreateView, UpdateView
from sampleapp.models import RenderMixinModel
from webplots.views import PreviewMixin


class New(PreviewMixin, CreateView):
    model = RenderMixinModel
    fields = ["name"]
    success_url = "/"

    preview_object = None

    def render_preview(self, context):
        return "foobar"


class Update(PreviewMixin, UpdateView):
    model = RenderMixinModel
    fields = ["name"]
    success_url = "/"

    def render_preview(self, context):
        return "foobar"
