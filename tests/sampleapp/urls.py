from django.conf.urls import url
from django.contrib import admin
from sampleapp.views import New, Update

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r"^new$", New.as_view(), name="new"),
    url(r"^(?P<pk>\d+)/edit$", Update.as_view(), name="update"),
]
