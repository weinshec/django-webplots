from django.db import models
from webplots.models import RenderMixin


class RenderMixinModel(RenderMixin, models.Model):
    name = models.CharField(max_length=128)
