import base64
import matplotlib.pyplot as plt
from io import BytesIO


plt.switch_backend('AGG')


def fig2png(fig):
    io = BytesIO()
    fig.savefig(io, format="png")
    return io.getvalue()


def fig2base64(fig):
    io = BytesIO()
    fig.savefig(io, format='png')
    return base64.encodebytes(io.getvalue()).decode("utf-8")


def fig2pdf(fig):
    io = BytesIO()
    fig.savefig(io, format='pdf', bbox_inches="tight")
    return io.getvalue()
