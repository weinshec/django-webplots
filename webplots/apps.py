from django.apps import AppConfig


class WebplotsConfig(AppConfig):
    name = 'webplots'
