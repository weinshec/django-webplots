import matplotlib.pyplot as plt
from django.core.files.base import ContentFile
from django.db import models
from django.dispatch import receiver
from uuid import uuid4
from webplots import fig2base64, fig2png, fig2pdf


class RenderMixin(models.Model):
    render = models.ImageField(upload_to="webplots/", blank=True)

    class Meta:
        abstract = True

    def save(self, *args, render=True, **kwargs):
        if render:
            self.remove_render()
            self.render = ContentFile(self.render_to("png"),
                                      "{}.png".format(uuid4().hex))

        super(RenderMixin, self).save(*args, **kwargs)

    def remove_render(self):
        self.render.delete(save=False)

    def draw(self, *args, **kwargs):
        fig, ax = plt.subplots()
        ax.text(0.5, 0.5, "Override `draw()` in your Model", ha="center")
        return fig

    def render_to(self, to, *args, **kwargs):
        known_formats = {
            "base64": fig2base64,
            "png": fig2png,
            "pdf": fig2pdf,
        }

        if to not in known_formats.keys():
            raise NotImplementedError("Unkown target format: {}".format(to))

        renderer = known_formats[to]

        fig = self.draw(*args, **kwargs)
        target = renderer(fig)
        plt.close(fig)

        return target


@receiver(models.signals.post_delete)
def remove_render_file_after_deletion(sender, **kwargs):
    if issubclass(sender, RenderMixin):
        sender.remove_render(kwargs["instance"])
