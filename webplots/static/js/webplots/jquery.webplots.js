(function($) {
  "use strict";

  var pluginName = 'webplots';


  var Webplots = function(el, options) {
    var _this = this;

    // Defaults:
    this.opts = $.extend({}, Webplots.defaults, options);
    // Store form element
    this.$form = $(el);
    // Bind form submission event
    this.bindSubmit();
  };


  Webplots.defaults = {
    action: "preview",                      // preview button action
    target_url: "",                         // form action url
    id_preview: "#id_preview",              // preview img tag id
    show_errors: true,                      // populate errors
    errors_selector: "div[id$='_errors']"   // error wrapper selector
  };


  Webplots.prototype.bindSubmit = function() {
    var _this = this;

    this.$form.on("submit", function(event) {
      var $action = $(document.activeElement).val();

      if ($action == _this.opts.action) {
        event.preventDefault();
        _this.ajaxPreview();
      }

    });
  };


  Webplots.prototype.ajaxPreview = function() {
    var _this = this;

    $.ajax({
      target_url : this.opts.target_url,
      type : "POST",
      dataType: "html",
      data : this.$form.serialize() + "&action=" + this.opts.action,

      success : function(data) {
        var response = jQuery(data);
        var preview = response.find(_this.opts.id_preview);
        var errors = response.find(_this.opts.errors_selector);

        $(_this.opts.id_preview).replaceWith(preview);

        if (_this.opts.show_errors == true) {
          errors.each(function(i) {
            var id = $(this).attr("id");
            $("#"+id).replaceWith(this);
          });
        }
      },
    });
  };

  $.fn[pluginName] = function(options) {
    return new Webplots(this, options);
  };

})(jQuery);
