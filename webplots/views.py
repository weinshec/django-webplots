from django.views.generic.edit import ModelFormMixin
from webplots import fig2base64


class PreviewMixin(ModelFormMixin):

    preview_name_suffix = "_preview"

    def post(self, request, *args, **kwargs):
        self.object = self.get_preview_object()
        if request.POST.get("action") == "preview":
            context = self.add_preview_to_context(self.get_context_data())
            return self.render_to_response(context)
        return super().post(request, *args, **kwargs)

    def add_preview_to_context(self, context):
        model_class = self.get_model_class()
        context_object_name = "{}{}".format(
            self.get_context_object_name(model_class()),
            self.preview_name_suffix)
        try:
            preview = "data:image/png;base64,{}".format(
                fig2base64(self.render_preview(context)))
            context.update({context_object_name: preview})
        finally:
            return context
        return context

    def get_model_class(self):
        if self.model is not None:
            model = self.model
        elif hasattr(self, 'object') and self.object is not None:
            model = self.object.__class__
        else:
            model = self.get_queryset().model
        return model

    def render_preview(self, context):
        raise NotImplementedError

    def get_preview_object(self):
        if hasattr(self, 'preview_object'):
            return self.preview_object
        return (hasattr(self, 'get_object') and self.get_object() or
                getattr(self, 'object', None))
